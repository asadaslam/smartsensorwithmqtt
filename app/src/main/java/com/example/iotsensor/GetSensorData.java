package com.example.iotsensor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class GetSensorData extends MainActivity implements SensorEventListener {

    private static final String TAG = "GetSensorData";
    public int selectedFrequency;
    MqttAndroidClient client;
    String topicName = "foo/acc";
    Sensor accelerometer;
    TextView xValue, yValue, zValue, combinedValue, timestamp;
    private SensorManager sensorManager;
    private SensorEvent event;
    private HandlerThread mSensorThread;
    private Handler mSensorHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_sensor_data);
        mqttClient();// Register the client for MQTT
        xValue = (TextView) findViewById(R.id.xValue);
        yValue = (TextView) findViewById(R.id.yValue);
        zValue = (TextView) findViewById(R.id.zValue);
        combinedValue = (TextView) findViewById(R.id.combinedValue);
        timestamp = (TextView) findViewById(R.id.timestamp);
        Log.d(TAG, "onCreate: Initializing Sensor Service");

        /*Adding Handler to run every event in each thread */


        mSensorThread = new HandlerThread("Sensor thread", Thread.MAX_PRIORITY);
        mSensorThread.start();
        mSensorHandler = new Handler(mSensorThread.getLooper());

        
        int sFreq = getIntent().getIntExtra("selectedFrequency", selectedFrequency);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //sensorManager.registerListener(GetSensorData.this, accelerometer, getFrequency());
        sensorManager.registerListener(GetSensorData.this, accelerometer, sFreq,mSensorHandler); //Register listener on the user defined sampling frequency (in microseconds)
        Log.d(TAG, "onCreate: Registered Accelerometer");

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        Log.d(TAG, "clientID: " + client.getClientId());
//        Log.d(TAG, "OnSensorChanged Called");
        publishEvent(event);
        //Log.d(TAG, "Message published at: " + event.timestamp);

    }

    /*Registration of MQTT and subscribtion of topic to receive sensor data*/
    public void mqttClient() {

        String clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(this.getApplicationContext(), "tcp://broker.hivemq.com:1883", clientId);
        try {
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Toast.makeText(GetSensorData.this, "Connected", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onSuccess");
                    setSubscribtion();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Toast.makeText(GetSensorData.this, "Not Connected", Toast.LENGTH_SHORT).show();

                    Log.d(TAG, "onFailure");

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
        client.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                Toast.makeText(GetSensorData.this, "Lost Connection", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {

                JSONObject jsonObject = new JSONObject(new String(message.getPayload()));
                String strAcceleration = jsonObject.getString("acceleration");
                String strTimestamp = jsonObject.getString("timestamp");

                //Log.d(TAG," Event Timestamp: " + new Double(strTimestamp).longValue()/1000000000);
//                long secondTimestamp = new Double(strTimestamp).longValue()/1000000000;
//                long HourTimestamp =    secondTimestamp/1000000000;
//                Log.d(TAG," Event Timestamp in hour: " + HourTimestamp);
                //Log.d(TAG," Event Timestamp in microSeconds : " + TimeUnit.SECONDS.convert(new Double(strTimestamp).longValue()/1000000000, TimeUnit.NANOSECONDS));
                //Log.d(TAG," Local Timestamp in microSeconds : " + TimeUnit.HOURS.convert(System.nanoTime()+2, TimeUnit.NANOSECONDS));

                if (xValue == null) {

                    Log.d(TAG,"Event Received at: "+  strTimestamp +" and acceleration is: "+strAcceleration );
                    xValue.setText(strAcceleration);
                } else {
                    xValue.append("\n");
                    Log.d(TAG,"Event Received at: "+  strTimestamp +" and acceleration is: "+strAcceleration );
                    xValue.append(strAcceleration);
                    xValue.setMovementMethod(new ScrollingMovementMethod());
                }

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
    }

    public void setSubscribtion() {
        try {
            client.subscribe(topicName, 0);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void publishEvent(SensorEvent event) {
        try {
            String stringEvent = Double.toString(Math.sqrt(event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2]));
            String stringEventTime = Double.toString(event.timestamp);
            if (client.isConnected() == true) {
                //Log.d(TAG, "Client is connected");
                String topic = topicName; // Topic for event
                /*Sending Message as JSON object*/
                JSONObject eventJSON = new JSONObject();
                try {
                    eventJSON.put("acceleration",stringEvent);
                    eventJSON.put("timestamp",stringEventTime);
                    //Log.d(TAG,"JSON message" + eventJSON.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                MqttMessage toBeSendMessage = new MqttMessage();
                client.publish(topic,eventJSON.toString().getBytes(), 0, false);
            } else {

                Log.d(TAG, "Client is not connected");
                 }


        } catch (MqttException e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


}
